# Penny

Beta release version of 'Penny' - personal assistant chat bot - by Skippy corp.

## Usage

1. Make sure you already install Flutter SDK and emulator
2. Run your emulator
3. Start app with `flutter run`

## Feature

1. Notification with snooze option and custom sound
2. Checking and display message
3. Show activities
4. Search contact
5. Search app
6. Task record
7. Being search engine
8. Fetching user data
9. Generate statistic
10. Multilanguage
11. Speech Recognition
12. Ask for ambiguity
