import 'package:flutter/material.dart';

import './pages/main.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Penny',
      routes: {'/': (context) => Main()},
    );
  }
}
