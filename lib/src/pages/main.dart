import 'package:flutter/material.dart';

import './main/appbar.dart';
import './main/conversations.dart';
import './main/field.dart';

class Main extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Main();
  }
}

class _Main extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppbar(),
      body: Container(
        child: Column(
          children: <Widget>[
            Conversations(),
            Field(),
          ],
        ),
      ),
    );
  }
}
