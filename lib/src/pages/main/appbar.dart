import 'package:flutter/material.dart';

Widget buildAppbar() {
  return AppBar(
    backgroundColor: Color.fromRGBO(151, 109, 208, 1),
    leading: IconButton(
      icon: Icon(Icons.menu),
      onPressed: () {},
    ),
    title: Text('Penny'),
    actions: <Widget>[
      IconButton(
        icon: Icon(Icons.search),
        onPressed: () {},
      )
    ],
  );
}
